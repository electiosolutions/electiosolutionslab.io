---
layout: post
title:  "Message from the train"
date:   2019-04-08 18:55:00 -0300
categories: pub,train
---

So I've been for a beer with a mate, and am on a train hone. Clearly this is not a commuter train because its full of tourists. Young kids run about, and hang off the bars. Have I got on the wrong train?, I question myself...

Nope, its just the 7pm train after the business rush has happened.

I engross myself in my iphone, using a great App which doesn't require me to be online, to operate.

I hate it when people say that phones are 'always on'. its bollocks. My phone is more like rarely on! Dont get me wrong, when my phone connects, its quick, but I like Apps which cater for the 'occasional on' and allows you to contine, despite a brief interruption.
